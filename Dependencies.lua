
-- VoxelEngine Dependencies

IncludeDir = {}
IncludeDir["glfw"] = "%{wks.location}/Engine/vendor/glfw"
IncludeDir["glm"] = "%{wks.location}/Engine/vendor/glm"
IncludeDir["imgui"] = "%{wks.location}/Engine/vendor/imgui"